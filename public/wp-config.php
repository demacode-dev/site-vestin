<?php
define('WPLANG', 'pt_BR'); // Idioma padrão do site
define('AUTH_KEY',         'kZgVcO)3445rlF|6^*D)p!X;BG ]|-U=Q$a%nHw,!b;;`pP#pIJ,:>u1/Uk/uwbE');
define('SECURE_AUTH_KEY',  'z!@m$i(N+$uG+<q{5y,Tj47oBQ(PhI!g$;{b,:oC-$4T+$6Yn1J-z*Y|nY2,-+jy');
define('LOGGED_IN_KEY',    '<;`-WQlcDdNX.>4^8lk]>!yvVBZa7~3-s=~G]th_F)9ER$+ P^j5A<]b2z#1C8q-');
define('NONCE_KEY',        'v^8W|ucMZZ4{nWqEdN|fF3P_YZqu}oihoDk-Dj87 ~xn{em}rU8lc%LKY)V2r8$|');
define('AUTH_SALT',        '3yFCMO{bz+HcZs[rne jncmP$u-JBUyQ`i)h|j:#-VB|%$V|<Q1u5&kR6GHbvjE^');
define('SECURE_AUTH_SALT', '/q;=-*Or`aTmmc^Dsw#A>saAz553a+K5t,3H/TNUR|`jo|5*!3C+b-ys!g$V@)t&');
define('LOGGED_IN_SALT',   '* AU~a&t:>[$H-HRZKu>HiC_+Z2aMn+J8|bGFuU}$C;7RO+]_!y|jn&4och5BW@G');
define('NONCE_SALT',       ']>9]<|V51IPK9{`W|Il.LXreY7!aZ}opX$NO>{evW YAZo7IUqqJN~J;ZKu*h)!^');

// Are we in SSL mode?
if (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
    $_SERVER['HTTPS'] = 'on';
}

if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
    $protocol = 'https://';
} else {
    $protocol = 'http://';
}

// Define site host
if (isset($_SERVER['X_FORWARDED_HOST']) && !empty($_SERVER['X_FORWARDED_HOST'])) {
    $hostname = $_SERVER['X_FORWARDED_HOST'];
} else {
    $hostname = $_SERVER['HTTP_HOST'];
}

// Define WordPress Site URLs if not already set in config files
if ( ! defined('WP_SITEURL')) {
    define('WP_SITEURL', $protocol . rtrim($hostname, '/'));
}

if ( ! defined('WP_HOME')) {
    define('WP_HOME', $protocol . rtrim($hostname, '/'));
}

// Try environment variable 'WP_ENV'
if (getenv('WP_ENV') !== false) {
    // Filter non-alphabetical characters for security
    define('WP_ENV', preg_replace('/[^a-z]/', '', getenv('WP_ENV')));
}

// Define o WP_ENV de acordo com a URL do site
if (!defined('WP_ENV')) {
    switch ($hostname) {
        case 'vestin.localhost': // Endereço local
        case '192.168.25.11': // Endereço local
            define('WP_ENV', 'local');
            break;

        case 'vestin.demacode.com.br': // Endereço teste
            define('WP_ENV', 'teste');
            break;

        case 'vestin.com.br': // Endereço live
        case 'www.vestin.com.br': // Endereço live
        default:
            define('WP_ENV', 'live');
    }
}

//define('DB_CHARSET', 'utf8');
//define('DB_COLLATE', '');
$table_prefix  = 'wp_'; // Prefixo no nome das tabelas.
define('EMPTY_TRASH_DAYS', 3); // Quantos dias um post deletado vai ficar na lixeira
define('WP_POST_REVISIONS', 3); // Quantos dias vai ficar o histórico de alterações de um post
define ('WP_MEMORY_LIMIT', '1024M');
define ('WP_MAX_MEMORY_LIMIT', '2048M');
define( 'WP_DEFAULT_THEME', 'nova-etiquetas' ); // Tema padrão

switch (WP_ENV) {
    case 'local': // Endereço local
        define('DB_NAME', 'homolog-site-vestin');
        define('DB_USER', 'demacode');
        define('DB_PASSWORD', 'D3m4c0de.');
        define('DB_HOST', 'demabase.cfbh2iojpdxg.sa-east-1.rds.amazonaws.com');
        define('WP_DEBUG', true);
        define('WP_DEBUG_LOG', true);
        define('WP_DEBUG_DISPLAY', true);
        define('WP_CACHE', false);
        break;

    case 'teste': // Endereço teste
        define('DB_NAME', 'homolog-site-vestin');
        define('DB_USER', 'demacode');
        define('DB_PASSWORD', 'D3m4c0de.');
        define('DB_HOST', 'demabase.cfbh2iojpdxg.sa-east-1.rds.amazonaws.com');
        define('WP_DEBUG', true);
        define('WP_DEBUG_LOG', true);
        define('WP_DEBUG_DISPLAY', true);
        define('WP_CACHE', false);
        break;

    case 'live': // Endereço live
    default:
        define('DB_NAME', 'prod-site-vestin');
        define('DB_USER', 'vestin');
        define('DB_PASSWORD', 'vestin123@');
        define('DB_HOST', 'demabase.cfbh2iojpdxg.sa-east-1.rds.amazonaws.com');
        define('WP_DEBUG', false);
        define('WP_DEBUG_LOG', false);
        define('WP_DEBUG_DISPLAY', false);
        define('WP_CACHE', false);
}

// Clean up
unset($hostname, $protocol);

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', __DIR__ . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define('DISALLOW_FILE_EDIT', true); 
define ('DISALLOW_FILE_MODS', true);
define('FS_METHOD', 'direct');

// Enganamos o ZenCache :D
// define('WP_CACHE', TRUE);


