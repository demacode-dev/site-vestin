	</main>

</div>
	<footer class="footer">
		<div class="container-background">
			<img src="<?=get_field('bloco_contato')['imagem_background']['url']?>">
		</div>
		<div class="container-contato-pai" id="contato">
			<div class="container-padrao-contato-pai">
				<div class="container-padrao-contato">
					<div class="container-contato anime anime-left">
						<div class="container-logo">
							<img src="<?= !empty(get_field('bloco_contato')['imagem_logo']['url']) ? get_field('bloco_contato')['imagem_logo']['url'] : get_stylesheet_directory_uri().'/img/logo-vestin.svg'?>">
						</div>
						<div class="email">
							<h1>Email</h1>
							<p><?=get_field('bloco_contato')['email']?></p>
						</div>
						<div class="telefone-texto">
							<h1>Telefone</h1>
							<p><?=get_field('bloco_contato')['telefone']?></p>
						</div>
						<div class="endereco">
							<h1>Endereço</h1>
							<p><?=get_field('bloco_contato')['endereco']?></p>
						</div>
					</div>
					<div class="container-formulario anime anime-right"">
						<h1>Alguma duvida?</h1>
						<p>Entre em contato através do formulário abaixo.</p>
						<form data-hc-form method="post" action="<?php echo admin_url('admin-ajax.php'); ?>">
							<div class="container-nome">
								<input required hc-mail-message type="text" name="name" placeholder="Nome e Sobrenome*">
								<input required hc-mail-message type="email" name="email" placeholder="Seu e-mail*">
							</div>
							<div class="container-textarea">
								<textarea hc-mail-message name="mensagem" placeholder="Sua mensagem aqui…"></textarea>
							</div>
							<button type="submit">Enviar <img src="<?=get_stylesheet_directory_uri()?>/img/right-arrow.svg"></button>
							<span data-hc-feedback></span>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="container-footer">
			<div class="container-padrao">
				<div class="container-copy">
					<p><?=get_field('copyright', 'footer')?> • Desenvolvido por Demacode_</p>
				</div>
				<div class="container-redes">
					<?php
						$redes = get_field('redes_sociais', 'footer');
						if(!empty($redes)){
							$contadorRedes = count($redes);
							for($i =0; $i < $contadorRedes; $i++){
								$rede = $redes[$i];
					?>
						<a href="<?= $rede['link']?>"><img src="<?= $rede['icone']['url']?>"></a>
					<?php } } ?>
				</div>
			</div>
		</div>
	</footer>
    <?php wp_footer();?>
</body>
</html>
