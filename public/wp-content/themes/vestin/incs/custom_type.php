<?php
add_action( 'init', function(){

	$labelsRepresentantes = array(
        'name' => _x('Representantes', 'post type general name'),
        'singular_name' => _x('Representante', 'post type singular name'),
        'add_new' => _x('Adicionar Novo', 'Novo item'),
        'add_new_item' => __('Novo Item'),
        'edit_item' => __('Editar Item'),
        'new_item' => __('Novo Item'),
        'view_item' => __('Ver Item'),
        'search_items' => __('Procurar Itens'),
        'not_found' =>  __('Nenhum registro encontrado'),
        'not_found_in_trash' => __('Nenhum registro encontrado na lixeira'),
        'parent_item_colon' => '',
        'menu_name' => 'Representantes'
    );

	$argsRepresentantes = array(
		'labels' => $labelsRepresentantes,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'has_archive' => false,
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array('title')
	);
 
 	register_post_type( 'representantes' , $argsRepresentantes );
	
});