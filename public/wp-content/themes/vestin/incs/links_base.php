<?php
add_action('wp_enqueue_scripts', function () {
	$scriptVersion = "1.2";

	/******* CSS PADRÃO */
	wp_enqueue_style('css_base', get_stylesheet_directory_uri().'/src/css/base.min.css?version='.$scriptVersion, array(), null, false);

	/******* JAVASCRIPT PADRÃO */
	wp_register_script('main', get_stylesheet_directory_uri().'/src/js/main.min.js?version='.$scriptVersion, array('jquery'), null, true);
	wp_enqueue_script('main', array(), null, true);
	
    /*MÁSCARAS*/
    wp_enqueue_script('validar', get_stylesheet_directory_uri().'/js/plugins/jquery.validate.min.js', array(), false, true);
    wp_enqueue_script('mascara', get_stylesheet_directory_uri().'/js/plugins/jquery.mask.min.js', array(), null, true);

    wp_enqueue_script('dema_masks', get_stylesheet_directory_uri().'/js/plugins/demacode-masks.js', array('jquery'), null, true);


     wp_enqueue_style('slick_css', get_stylesheet_directory_uri().'/js/plugins/slick/slick.css');
	wp_enqueue_style('slick_theme_css', get_stylesheet_directory_uri().'/js/plugins/slick/slick-theme.css');
	wp_enqueue_script('slick_js', get_stylesheet_directory_uri().'/js/plugins/slick/slick.min.js', array(), null, true);

});
