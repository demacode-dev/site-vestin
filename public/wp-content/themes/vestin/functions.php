<?php
//******* LINKS PARA SCRIPTS CSS E JS */

/******* Scripts padrões *******/
include __DIR__."/incs/links_base.php"; 

/******* Links de plugins *******/
include __DIR__."/incs/links_plugins.php"; 


/******************************/


//****** CUSTOM */

/******* Custom config *******/
include __DIR__."/incs/custom_config.php";

/******* Custom image size *******/
include __DIR__."/incs/custom_image_sizes.php";

/******* Custom URL *******/
include __DIR__."/incs/custom_url.php";

//****** INFORMAÇÕES GLOBAIS

//include __DIR__."/incs/custom_info.php";


/******************************/

//******* ACF OPTIONS */

/******* Configurações para o acf options ********/
include __DIR__."/incs/acf_options.php";

/******* Custom post types *******/
include __DIR__."/incs/custom_type.php";

/******* Custom taxonomy *******/
include __DIR__."/incs/custom_tax.php";


function generateThumbnailImage( $image, $post_id  ){
    $upload_dir = wp_upload_dir();
    $image_data = file_get_contents($image['tmp_name']);
    $filename = $image['name'];

    if(wp_mkdir_p($upload_dir['path']))  {
        $file = $upload_dir['path'] . '/' . $filename;
    } else {
        $file = $upload_dir['basedir'] . '/' . $filename;
    }

    file_put_contents($file, $image_data);

    $wp_filetype = wp_check_filetype($filename, null );

    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'post_title' => sanitize_file_name($filename),
        'post_content' => '',
        'post_status' => 'inherit'
    );
    $attach_id = wp_insert_attachment( $attachment, $file, $post_id );
    require_once(ABSPATH . 'wp-admin/includes/image.php');
    $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
    $res1 = wp_update_attachment_metadata( $attach_id, $attach_data );

    if ( $post_id > 0 ) {
        $res2 = set_post_thumbnail( $post_id, $attach_id );
    }

    return $attach_id;
}