<?php  do_action('before_wphead_after_init');?>
<!DOCTYPE html>
<html <?php language_attributes();?> class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-163413155-1"></script>
	<script>
 	 window.dataLayer = window.dataLayer || [];
 	 function gtag(){dataLayer.push(arguments);}
 	 gtag('js', new Date());

 	 gtag('config', 'UA-163413155-1');
	</script>

	<?php
	HC::init();
	?>
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0" />
	<title><?php echo wp_title();?></title>
    <!-- FONT AWESOME - ICONES PERSONALIZADOS -->
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <!-- FONTE DO SITE -->
	<link rel="stylesheet" href="<?= get_template_directory_uri() ?>/src/css/header.min.css">
	<link rel="stylesheet" href="<?= get_template_directory_uri() ?>/src/css/footer.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">
	<?php wp_head();?>
	
	
</head>
<body id="body" class="scroll-block">
	<div class="container-loading" id="container-loading">
		<div class="conteudo loading">
		<img src="<?= !empty(get_field('logo_header','header')['url']) ? get_field('logo_header','header')['url'] : get_stylesheet_directory_uri().'/img/logo-vestin.svg'?>">
			<img id="gif" src="<?=get_stylesheet_directory_uri()?>/img/loading.gif">
		</div>
	</div>
	<header class="header">
		<div class="menu-lateral" id="menu-lateral">
			<div class="after-container">
				<div class="fechar-menu">
					<img onclick="abrirMenu()" src="<?php echo get_stylesheet_directory_uri(); ?>/img/cancel.svg" alt="">
					<a href="/"><img class="logo" src="<?= get_stylesheet_directory_uri()?>/img/logo-vestin.svg"></a>
					<div class="div-vazia"></div>
				</div>
				
				<div class="itens-responsivo">
				<?php
					$nomes = get_field('nomes_sessoes', 'header');
						if(!empty($nomes)){
							$contadorNomes = count($nomes);
							for($i =0; $i < $contadorNomes; $i++){
								$nome = $nomes[$i];
				?>
					<ul>
						<li><a onclick="scrollParaElemento( '<?= $nome['id_sessao']?>' )"> <?= $nome['nome_sessao']?> </a></li>
					</ul>
				<?php } } ?>	
				</div>
				<div class="container-redes-pai">
					<div class="container-redes">
						<?php
							$redes = get_field('redes_sociais_lateral', 'header');
							if(!empty($redes)){
								$contadorRedes = count($redes);
								for($i =0; $i < $contadorRedes; $i++){
									$rede = $redes[$i];
						?>
							<a href="<?= $rede['link']?>"><img src="<?= $rede['imagem']['url']?>"></a>
						<?php } } ?>
					</div>
					<p id="copy"><?=get_field('copyright', 'footer')?> • Desenvolvido por Demacode_</p>
				</div>	
			</div>
		</div>

	</header>
	<div id="navbar">
			<div class="container-padrao">
				<div class="container-logo">
				<img src="<?= !empty(get_field('logo_header','header')['url']) ? get_field('logo_header','header')['url'] : get_stylesheet_directory_uri().'/img/logo-vestin.svg'?>">
				</div>
				<div class="container-menu" onclick="abrirMenu()">
					<img src="<?=get_stylesheet_directory_uri()?>/img/menu.svg">
					<p>Menu</p>
				</div>
			</div>
		</div>
	<main>
