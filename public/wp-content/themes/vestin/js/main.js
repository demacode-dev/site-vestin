jQuery(document).ready(function($){
    $('#container-loading').fadeOut();
	$('#body').removeClass('scroll-block');
    $target = $('.anime'),
      animationClass = 'anime-init',
      windowHeight = $(window).height(),
	  offset = windowHeight - (windowHeight / 5);
	var root = document.documentElement;
	root.className += ' js';

	function boxTop(idBox) {
		var boxOffset = $(idBox).offset().top;
		return boxOffset;
	}

	function animeScroll() {
		var documentTop = $(document).scrollTop();
		$target.each(function() {
		if (documentTop > boxTop(this) - offset) {
			$(this).addClass(animationClass);
		}
		});
	}
	animeScroll();

	$(document).scroll(function() {
		$target = $('.anime');
		animeScroll();
	});
});

function abrirDropDown(){
	dropdown = document.getElementById('container-options');
	seta = document.getElementById('seta');
	if(dropdown.classList.contains('dropdown-ativo')){
		dropdown.classList.remove('dropdown-ativo');
		seta.classList.remove('dropdown-ativo');
	}else{
		dropdown.classList.add('dropdown-ativo');
		seta.classList.add('dropdown-ativo');
	}
}

function scrollParaElemento(elemento){
	document.getElementById('menu-lateral').classList.remove('mostrar-menu');
	setTimeout(function(){
		document.getElementById(elemento).scrollIntoView();
	}, 250) 
    
}

function abrirMenu(){
	var menuLateral = document.getElementById('menu-lateral');

	if(!menuLateral.classList.contains('mostrar-menu')) {
		menuLateral.classList.add('mostrar-menu');
			
	} else {
		menuLateral.classList.remove('mostrar-menu');
	}
}

window.onscroll = function() {myFunction()};

var navbar = document.getElementById("navbar");
var sticky = navbar.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    navbar.classList.add("sticky")
  } else {
    navbar.classList.remove("sticky");
  }
}
