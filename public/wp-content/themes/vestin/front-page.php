<?php get_header();
    wp_enqueue_style('css_front-page', get_stylesheet_directory_uri().'/src/css/front-page.min.css', array(), null, false);
?>
    <div class="container-carrossel-pai">
        <div class="container-redes">
            <div class="container-padrao-redes anime anime-up">
                <?php
                    $redesSociais = get_field('redes_sociais_lateral', 'header');
                    $contadorRedes = count($redesSociais);
                    for($i = 0; $i < $contadorRedes; $i++){
                        $rede = $redesSociais[$i];
                ?>
                    <a href="<?=$rede['link']?>" target="_blank"> <img src="<?=$rede['imagem']['url']?>"></a>
                <?php } ?>
            </div>
        </div>
        <div class="container-carrossel">
            <?php
                $carrossels = get_field('bloco_carrosel');
                $contadorCarrosel = count($carrossels);
                for($i = 0; $i < $contadorCarrosel; $i++){
                    $carrossel = $carrossels[$i];
            ?>
                <div class="container-banner">
                    <div class="container-background">
                        <div class="circulo"></div>
                        <img src="<?= $carrossel['imagem_background']['url'] ?>">
                    </div>
                    <div class="container-padrao anime anime-left">
                        <div class="container-conteudo">
                            <h1><?= $carrossel['titulo']?></h1>
                            <p> <?= $carrossel['texto']?></p>
                            <?php if( $carrossel['botao'] ){ ?>
                                <div class="container-botao">
                                    <a href="<?= $carrossel['link_botao'] ?>"> <?= $carrossel['texto_botao'] ?><img src="<?=get_stylesheet_directory_uri()?>/img/right-arrow.svg"></a>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="container-linhas-e-produtos" id="linhas-de-produto">
        <div class="container-circulo"></div>
        <div class="container-linhas-de-produtos">
            <div class="container-imagem-background">
                <img src="<?= get_field('bloco_linha_de_produtos')['imagem_background']['url']?>" id="imagemModelo">
            </div>
            <div class="container-padrao-linha-pai">
                <div class="container-padrao-linha">
                    <div class="container-texto anime anime-right">
                        <h1><?= get_field('bloco_linha_de_produtos')['titulo']?></h1>
                        <h2><?= get_field('bloco_linha_de_produtos')['subtitulo']?></h2>
                        <p><?= get_field('bloco_linha_de_produtos')['texto']?></p>
                    </div>
                    <div class="container-linhas">
                        <?php
                            $linhas = get_field('bloco_linha_de_produtos')['linha_de_produtos'];
                            $contadorLinhas = count($linhas);
                            for($i = 0; $i < $contadorLinhas; $i++){
                                $linha = $linhas[$i];
                                $url_imagem_modelo = $linha['imagem_modelo']['url'];
                        ?>
                            <div class="container-linha anime anime-fade" style="transition-delay: 0.<?=$i?>s">
                                <div class="container-conteudo-pai">
                                    <div class="container-imagem">
                                        <img src="<?= $linha['imagem']['url']?>">
                                    </div>
                                   
                                    <div class="container-conteudo" onmouseenter = "mudarImagemModelo( '<?= $url_imagem_modelo; ?>' )">
                                        <h1><?= $linha['titulo']?></h1>
                                        <p><?= $linha['texto']?></p>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-nossos-produtos" id="nossos-produtos">
            <div class="container-background"></div>
            <div class="container-padrao-produtos">
                <h1 class="anime anime-left"><?= get_field('bloco_produtos')['titulo']?></h1>
                <p class="anime anime-left"><?= get_field('bloco_produtos')['texto']?></p>
                <div class="container-produtos">
                    <?php
                        $produtos = get_field('bloco_produtos')['produtos'];
                        $contadorProdutos = count($produtos);
                        for($i = 0; $i < $contadorProdutos; $i++){
                            $produto = $produtos[$i];
                    ?>
                        <div class="container-produto anime anime-fade" style="transition-delay: 0.<?=$i?>s">
                            <img src="<?=$produto['imagem']['url']?>">
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="container-sobre-e-revenda" id="revenda">
        <div class="container-imagem-background">
            <img src="<?= get_field('bloco_revenda')['imagem_background']['url']?>">
        </div>
        <div class="container-revenda-pai">
            <div class="container-background">
                <div class="circulo-1"></div>
                <div class="circulo-2"></div>
            </div>
            <div class="container-padrao-revenda-pai">
                <h1 class="anime anime-left"><?= get_field('bloco_revenda')['titulo']?></h1>
                <p class="anime anime-left"><?= get_field('bloco_revenda')['texto']?></p>
                <div class="container-revenda anime anime-left">
                    <div class="container-para-revender">
                        <img src="<?= get_field('bloco_revenda')['para_revender']['imagem']['url']?>">
                        <h1><?= get_field('bloco_revenda')['para_revender']['titulo']?></h1>
                        <p><?= get_field('bloco_revenda')['para_revender']['texto']?></p>
                    </div>
                    <div class="container-sua-lojista anime anime-left">
                        <img src="<?= get_field('bloco_revenda')['sua_lojista']['imagem']['url']?>">
                        <h1><?= get_field('bloco_revenda')['sua_lojista']['titulo']?></h1>
                        <p><?= get_field('bloco_revenda')['sua_lojista']['texto']?></p>
                    </div>
                </div>
                <div class="container-whats">
                    <a class="anime anime-left" target="_blank" rel="noopener noreferrer" href="https://api.whatsapp.com/send?phone=<?= get_field('bloco_revenda')['numero_whatsapp']?>&text=Sua mensagem aqui">ENTRAR EM CONTATO <img src="<?=get_stylesheet_directory_uri()?>/img/whatsapp.svg"></a>
                </div>
            </div>
        </div>
        <div class="container-sobre" id="sobre-nos">
            <div class="container-padrao-sobre">
                <div class="container-conteudo anime anime-left"">
                    <h1><?= get_field('bloco_sobre_nos')['titulo']?></h1>
                    <p><?= get_field('bloco_sobre_nos')['texto']?></p>
                </div>
            </div>
        </div>
    </div>
    <div class="container-map-pai" id="localizacao">
        <div class="container-encontre">
            <p>Encontre nossos representantes</p>
        </div>
        <div class="container-seletor-pai">
            <div class="container-seletor">
                <div class="container-selected" onclick="abrirDropDown()">
                    <p id="texto-selected">Filtrar</p>
                    <div class="container-seta">
                        <img id="seta" src="<?php echo get_template_directory_uri()?>/img/play-solid.svg">
                    </div>
                </div>
                <div class="container-options" id="container-options">
                    <?php
                        $representantesArgs = array(
                            'post_type' => 'representantes',
                            'orderby' => 'name',
                            'post_status' => 'publish',
                            'order' => 'ASC',
                            'hide_empty' => false,
                            'posts_per_page' => -1
                        );
                        $representantes = get_posts($representantesArgs);
                        $contadorRepresentantes = count($representantes);
                        $marcadores = array();
                        for($i = 0; $i < $contadorRepresentantes; $i++){
                            $postRepresentante = $representantes[$i];
                            $booleanoSede = get_field('booleano_sede', $postRepresentante->ID);
                            $marcadores[$i]['nome'] = get_field('titulo', $postRepresentante->ID);
                            $marcadores[$i]['lat'] = get_field('latitude', $postRepresentante->ID);
                            $marcadores[$i]['lon'] = get_field('longitude', $postRepresentante->ID);
                            $marcadores[$i]['conjunto'] = $i;
                            
                    ?>
                        <div class="option" onclick="mapCenterController(<?=$i?>)"><?= get_field('titulo', $postRepresentante->ID)?></div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div id="map"></div>
    </div>
    
    <div class="modal-lightbox" id="modal">

        <div class="modal-content">
            <span class="close">&times;</span>
            <div class="modal-contatos">
                <div class="modal-contato">
                    <h1><?= get_field('bloco_lightbox_contato')['titulo']?></h1>
                    <p><?= get_field('bloco_lightbox_contato')['e-mail']?></p>
                    <p><?= get_field('bloco_lightbox_contato')['telefone']?></p>
                </div>
                
            </div>
            
        </div>
    
    </div>
</main>

    <script type="text/javascript">
        var map;
        var marcadores = <?= json_encode($marcadores); ?>;
        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: -14.235004, lng: -51.925282},
                zoom: 4,
                mapTypeControl: false,
                scaleControl: false,
                streetViewControl: false,
                fullscreenControl: false
            });


            adicionarMarcadores(marcadores);
        }
        function adicionarMarcadores(marcadores){
            let count = marcadores.length;
            var icon = {
                scaledSize: new google.maps.Size(45, 45), // scaled size
            };
                
            for(var i = 0; i < count; i++){
                var marcador = marcadores[i];
                var latitude = parseFloat(marcador.lat);
                var longitude = parseFloat(marcador.lon);
                var longitude = parseFloat(marcador.lon);
                var titulo = marcador.nome;
                var conjunto = marcador.conjunto;
                var marker = new google.maps.Marker({
                    position: {lat: latitude, lng: longitude},
                    title: titulo,
                    map: map,
                    icon: icon,
                    id: conjunto,
                });

                window.google.maps.event.addListener(marker, 'click', function () {
                    AbrirModal();
                });
            }

            
        }

        //modal
        var modal = document.getElementById("modal");

        function AbrirModal() {
            modal.style.display = "block";
        }

        var span = document.getElementsByClassName("close")[0];

        span.onclick = function() {
            modal.style.display = "none";
        }


        function mapCenterController(id){
            map.setCenter(new google.maps.LatLng(marcadores[id].lat, marcadores[id].lon));
            map.setZoom(6);
            abrirDropDown();
        }
        jQuery(document).ready(function($){ 
            $('.container-carrossel').slick({
                fade: true,
                dots: true,
                arrows: false,
                draggable: false
            }); 
        });
        
        function mudarImagemModelo(url) {
            if (document.getElementById("imagemModelo").src != url) {
                jQuery("#imagemModelo").fadeOut(300, function () {
                    document.getElementById("imagemModelo").src = url;
                }).fadeIn(300);
            }
        }

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyABBWliDekJqNzj6mCssYfzs-25pK2yIAQ&callback=initMap" async defer></script>
<?php get_footer(); ?>