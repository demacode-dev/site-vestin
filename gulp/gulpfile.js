var gulp = require('gulp');
var stylus = require('gulp-stylus'); // Usado para stylus
var uglify = require('gulp-uglify'); // usado para minificar js
var watch = require('gulp-watch'); // Usado para acompanhar os arquivo que são modificados
var rename = require('gulp-rename'); // Usado para renomear arquivos depois de minificar
var replace = require('gulp-replace'); // Usado para trocar textos dentro dos arquivos
var cleanCSS = require('gulp-clean-css');

var postcss = require('gulp-postcss');
var cssnext = require('postcss-cssnext');

//***** path base
var path = '../public/wp-content/themes/vestin/';

gulp.task('postcss', function () {
  // Cria variavel com os plugins do postcss
  var plugins = [
    cssnext({
        browsers: ['> 0.5%']
    })
  ];

  // Função que minifica e otimiza o css
  return gulp.src(path +'css/*.styl')
    .pipe(stylus({compress: false}))
    .pipe(cleanCSS())
    .pipe(replace('url("../', 'url("../../')) // Ajusta url das imagens
    .pipe(postcss(plugins)) // PostCss
    .pipe(rename({suffix: '.min'})) // Adiciona .min nos arquivos
    .pipe(gulp.dest(path +'src/css/'));
});

// JS
gulp.task('scripts', function() {
    return gulp.src(path +'js/*.js') // Local dos arquivos .js
    .pipe(uglify()) // minifica o arquivo .js
    .pipe(rename({suffix: '.min'})) // adiciona .min nos arquivos
    .pipe(gulp.dest(path +'src/js/')); // Local onde o arquivo js deve ser gerado
});

// HTML BASE
gulp.task('base_html', function() {
    gulp.src(path +'**/*.php');
});

// WATCH
gulp.task('watch', function() {
    // Informar local de cada arquivo para o watch acompanhar e executar as funções informadas
    gulp.watch([path +'css/*.styl'], ['postcss']);
    gulp.watch([path +'js/*.js'], ['scripts']);
    gulp.watch([path +'**/*.php'], ['base_html']);
});

gulp.task('default', ['watch']);
